apiVersion: batch/v1
kind: Job
metadata:
  name: "set-password-r{{ .Release.Revision }}"
spec:
  template:
    metadata:
      labels:
        job: set-password
    spec:
      restartPolicy: OnFailure
      initContainers:
        - name: await-startup
          image: "docker.io/curlimages/curl:8.12.1@sha256:94e9e444bcba979c2ea12e27ae39bee4cd10bc7041a472c4727a558e213744e6"
          command:
            - sh
            - -c
            - |
              until timeout 10 curl --fail http://sonarqube/; do
                echo "not yet up"
                sleep 5
              done
      containers:
        - name: set-password
          image: docker.io/library/postgres:17.2-alpine3.19@sha256:d98de4f4959fb6b4531f3fb0cfe92aa49f799e5ebc7a086ae7775b59e7d534e7
          env:
            - name: PGPASSWORD
              valueFrom:
                secretKeyRef:
                  name: sonar.postgres.credentials.postgresql.acid.zalan.do
                  key: password
          command:
            - ash
            - -euxo
            - pipefail
            - -c
            - |
              set_password(){
                psql -d sonar -U sonar -h postgres \
                  -c "update users set crypted_password='100000\$eAP4z5e9o6OgGX4nY41QI7LrmA1mK6UnIua4xrYjeBfxW2n0voXegzKqhCfGmC92AP3PBwUGo5DbRbKVFuWPNA==', salt='WMbxTJSfVaDFYvF1kXxG2R6nCzE=', reset_password='false', hash_method='PBKDF2' where login='admin';"
              }
              until set_password; do
                echo "setting password failed"
                sleep 5
              done
              echo "password is set"
